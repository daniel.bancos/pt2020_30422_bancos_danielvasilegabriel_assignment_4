package mvc;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

import bll.Order;

public class ChefView extends JFrame implements Observer{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton quitBtn = new JButton("Quit");
	private JPanel mainContent = new JPanel();
	private JPanel pane1 = new JPanel();
	
	DefaultListModel modelOrderList = new DefaultListModel();
	private JList orderList = new JList(modelOrderList);
	
	ChefView(){
		
		pane1.setLayout(null);
		pane1.setBackground(Color.ORANGE);
		pane1.setBounds(25, 30, 945, 300);
		pane1.add(orderList);
		orderList.setBounds(220, 50, 500, 200);
		orderList.setBorder(BorderFactory.createLineBorder(Color.black));
		
		mainContent.setLayout(null);
		mainContent.setBackground(Color.GRAY);
		
		quitBtn.setBounds(25, 645, 60, 20);
		
		mainContent.add(pane1);
		mainContent.add(quitBtn);
		
		this.setSize(1000, 700);
		this.setResizable(false);
		this.setPreferredSize(this.getSize());
		this.setContentPane(mainContent);
		this.pack();
		
		this.setLocationRelativeTo(null);
		this.setTitle("Admin");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	void addBackListener(ActionListener action) {
		quitBtn.addActionListener(action);
	}
	
	void addWindowCloseListener(WindowListener action) {
		this.addWindowListener(action);
	}

	public void update(Observable o, Object info) {
	  ArrayList intel = (ArrayList) info;
		Order order = (Order)intel.get(0);
		ArrayList<String> items = (ArrayList<String>)intel.get(1);
		String entry = "Order #" + order.getId() + "("+ order.getDate() +"): ";
		for(String st: items) {
			entry += st + ", ";
		}
		entry = entry.substring(0, entry.length() - 2);
		System.out.println(entry);
		modelOrderList.addElement(entry);
		
	}
}
