package mvc;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import bll.IRestaurantProcessing;
import bll.MenuItem;

public class WaiterView extends JFrame implements IRestaurantProcessing{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton quitBtn = new JButton("Quit");
	private JButton createOrderBtn = new JButton("Create");
	private JButton computePriceBtn = new JButton("Total");
	private JButton moveItemBtn = new JButton(">>");
	private JButton removeItemBtn = new JButton("X");
	private JButton generateBillBtn = new JButton("Bill");
	
	private JPanel mainContent = new JPanel();
	private JPanel pane1 = new JPanel();
	private JPanel pane2 = new JPanel();
	private JPanel rightPanel = new JPanel();
	
	private JLabel orderTableLabel = new JLabel("Table");
	private JLabel orderDateLabel = new JLabel("Date");
	
	private JTextField orderTableTf = new JTextField();
	private JTextField orderDateTf = new JTextField();
	private JTextField priceTf = new JTextField();
	
	DefaultListModel modelMenuItemsList = new DefaultListModel();
	DefaultListModel modelOrderItemsList = new DefaultListModel();
	DefaultListModel modelOrderComponentsList = new DefaultListModel();
	private JList menuItemsList = new JList(modelMenuItemsList);
	private JList orderItemsList = new JList(modelOrderItemsList);
	private JList orderComponentsList = new JList(modelOrderComponentsList);
	
	DefaultTableModel tableModel;
	private JTable ordersTable = new JTable();
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	WaiterView(){
		
		pane1.setLayout(null);
		pane1.setBackground(Color.LIGHT_GRAY);
		pane1.setBounds(25, 30, 500, 295);
		pane1.add(orderTableLabel);
		pane1.add(orderDateLabel);
		pane1.add(orderTableTf);
		pane1.add(orderDateTf);
		pane1.add(menuItemsList);
		pane1.add(orderItemsList);
		pane1.add(moveItemBtn);
		pane1.add(removeItemBtn);
		
		orderTableLabel.setBounds(10, 40, 50, 20);
		orderTableTf.setBounds(65, 40, 50, 25);
		
		orderDateLabel.setBounds(10, 70, 50, 20);
		orderDateTf.setBounds(65, 70, 125, 25);
		orderDateTf.setEditable(false);
		orderDateTf.setText(dateFormat.format(new Date()));
		
		menuItemsList.setBounds(10, 100, 180, 185);
		orderItemsList.setBounds(310, 100, 180, 185);
		moveItemBtn.setBounds(200, 140, 100, 50);
		removeItemBtn.setBounds(200, 200, 100, 50);
		
		pane2.setLayout(null);
		pane2.setBackground(Color.orange);
		pane2.setBounds(25, 345, 500, 295);
		pane2.add(createOrderBtn);
		pane2.add(computePriceBtn);
		pane2.add(generateBillBtn);
		pane2.add(priceTf);
		createOrderBtn.setBounds(30, 30, 130, 60);
		computePriceBtn.setBounds(30, 110, 130, 60);
		generateBillBtn.setBounds(30, 190, 130, 60);
		priceTf.setBounds(170, 110, 100, 60);
		priceTf.setFont(new Font("Arial", Font.PLAIN, 25));
		priceTf.setEditable(false);

		rightPanel.setLayout(null);
		rightPanel.setBackground(Color.DARK_GRAY);
		rightPanel.setBounds(550, 30, 420, 610);
		ordersTable.setBounds(40, 40, 340, 360);
		ordersTable.setBackground(Color.LIGHT_GRAY);
		tableModel = new DefaultTableModel() {
			@Override
	    public boolean isCellEditable(int row, int column) {
	       //all cells false
	       return false;
	    }
		};
		rightPanel.add(ordersTable);
		rightPanel.add(orderComponentsList);
		orderComponentsList.setBounds(40, 410, 340, 120);
		ordersTable.setModel(tableModel);
		tableModel.addColumn("id");
		tableModel.addColumn("Date");
		tableModel.addColumn("Table");
		
		mainContent.setLayout(null);
		mainContent.setBackground(Color.GRAY);
		
		quitBtn.setBounds(25, 645, 60, 20);
		
		mainContent.add(pane1);
		mainContent.add(pane2);
		mainContent.add(rightPanel);
		mainContent.add(quitBtn);
		
		this.setSize(1000, 700);
		this.setResizable(false);
		this.setPreferredSize(this.getSize());
		this.setContentPane(mainContent);
		this.pack();
		
		this.setLocationRelativeTo(null);
		this.setTitle("Waiter");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public int createBaseMenuItem(String name, float price) {
		// TODO Auto-generated method stub
		return 0;
	}

	public MenuItem createCompositeMenuItem(String name, float price, ArrayList<String> items) {
		// TODO Auto-generated method stub
		return null;
	}

	public void editMenuItem() {
		// TODO Auto-generated method stub
		
	}

	public void deleteMenuItem(String name, ArrayList<String> itemsToRemove) {
		// TODO Auto-generated method stub
		
	}

	public void createNewOrder(int id, int table, String date, ArrayList<String> items) {
		tableModel.addRow(new Object[] {String.valueOf(id), date, String.valueOf(table)});
		
	}

	public float computeOrderPrice(int id, float price) {
		priceTf.setText(String.valueOf(price));
		return 0;
	}

	public void generateBill(String id, String date, float price) {
		JOptionPane.showMessageDialog(null, "Bill successfully generated", "Message", 1);
		
	}

	public int editItems(String name, String baseName, String compositeName, float price, ArrayList<String> components) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void initView(ArrayList<MenuItem> items) {
		modelMenuItemsList.removeAllElements();
		for(MenuItem item: items) {
			modelMenuItemsList.addElement(item.getName());
		}
	}
	
	public int getSelectedId() {
		int row = ordersTable.getSelectedRow();
		if(row == -1) {
			return -1;
		}
		return Integer.parseInt(tableModel.getValueAt(row, 0).toString());
	}
	
	public String getSelectedDate() {
		int row = ordersTable.getSelectedRow();
		if(row == -1) {
			return null;
		}
		return tableModel.getValueAt(row, 1).toString();
	}
	
	public String getTable() {
		return orderTableTf.getText();
	}
	
	public String getDate() {
		return orderDateTf.getText();
	}
	
	public String getProduct() {
		if(!menuItemsList.isSelectionEmpty()) {
			return menuItemsList.getSelectedValue().toString();
		} else {
			return null;
		}
	}
	
	public void addProduct(String item) {
		modelOrderItemsList.addElement(item);
	}
	
	public boolean alreadyExists(String product) {
		
		for(int i = 0; i < modelOrderItemsList.getSize(); i++) {
			String item = (String) modelOrderItemsList.getElementAt(i);
			if(item.equals(product)) {
				return true;
			}
		}
		return false;
	}
	
	public ArrayList<String> getMenuItems(){
		ArrayList<String> toReturn = new ArrayList();
		
		for(int i = 0; i < modelOrderItemsList.getSize(); i++) {
			String item = modelOrderItemsList.getElementAt(i).toString();
			toReturn.add(item);
		}
		
		return toReturn;
	}
	
	public void removeListItem() {
		int index = orderItemsList.getSelectedIndex();
		if(index != -1) {
			modelOrderItemsList.remove(index);
		}
	}
	
	public void addToOrderComponentsList(ArrayList<String> components) {
		modelOrderComponentsList.removeAllElements();
		for(String st: components) {
			modelOrderComponentsList.addElement(st);
		}
	}
	
	void addBackListener(ActionListener action) {
		quitBtn.addActionListener(action);
	}
	
	void addMoveMenuItemListener(ActionListener action) {
		moveItemBtn.addActionListener(action);
	}
	
	void addCreateOrderListener(ActionListener action) {
		createOrderBtn.addActionListener(action);
	}
	
	void addComputePriceListener(ActionListener action) {
		computePriceBtn.addActionListener(action);
	}
	
	void addRemoveMenuItemListener(ActionListener action) {
		removeItemBtn.addActionListener(action);
	}
	
	void addShowOrderComponentsListener(MouseListener action) {
		ordersTable.addMouseListener(action);
	}
	
	void addGenerateBillListener(ActionListener action) {
		generateBillBtn.addActionListener(action);
	}
	
	void addWindowCloseListener(WindowListener action) {
		this.addWindowListener(action);
	}
}
