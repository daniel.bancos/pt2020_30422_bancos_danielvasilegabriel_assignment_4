package mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import bll.MenuItem;
import bll.Order;
import bll.Restaurant;

public class Controller {
	private MainView mainView;
	private AdministratorView adminView = new AdministratorView();
	private WaiterView waiterView = new WaiterView();
	private ChefView chefView = new ChefView();
	
	private int counter = 1;
	
	Restaurant restaurant;
	
	Controller(MainView mainView, Restaurant newRestaurant){
		this.mainView = mainView;
		restaurant = newRestaurant;
		restaurant.addObserver(chefView);
		initAdminView();
		initWaiterView();
		
		mainView.addAdminBtnListener(new AdminBtnListener());
		mainView.addWaiterBtnListener(new WaiterBtnListener());
		mainView.addChefBtnListener(new ChefBtnListener());
		mainView.addWindowCloseListener(new WindowCloseListener());
	}
	
	public static void main(String args[]) throws EOFException {
		MainView mainView= new MainView();
//		AdministratorView adminView = new AdministratorView();
//		WaiterView waiterView = new WaiterView();
//		ChefView chefView = new ChefView();
		Restaurant restaurant = new Restaurant();
		Restaurant rest = null;
		
		int check = 0;
		FileInputStream fileInputStream;
		try {
			fileInputStream = new FileInputStream("menu.txt");
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
		  rest = (Restaurant) objectInputStream.readObject();
		  objectInputStream.close(); 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			check = 1;
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		if(check == 0) {
			restaurant = rest;
		}
		
		Controller controller = new Controller(mainView, restaurant);
		
		mainView.setVisible(true);
	}
	
	class AdminBtnListener implements ActionListener{

		public void actionPerformed(ActionEvent arg) {
			//adminView = new AdministratorView();
			mainView.setVisible(false);
			//adminView.initView(restaurant.getRestaurantMenu());
			adminView.setVisible(true);
			adminView.addBackListener(new AdminBackListener());
			adminView.addMoveProductListener(new MoveProductListener());
			adminView.addRemoveProductListener(new RemoveProductListener());
			adminView.addAddBaseProdListener(new AddBaseProductListener());
			adminView.addDeleteBaseProdListener(new DeleteBaseProductListener());
			adminView.addAddCompoisteProdListener(new AddCompositeProductListener());
			adminView.addCompositeItemSelectedListener(new CompositeItemSelectedListener());
			adminView.addCompositeItemSelectedListener(new FillFieldsOnSelect());
			adminView.addEditItemListener(new EditItemListener());
			adminView.addWindowCloseListener(new WindowCloseListener());
		}
		
	}
	
	class WaiterBtnListener implements ActionListener{

		public void actionPerformed(ActionEvent arg) {
			waiterView.initView(restaurant.getRestaurantMenu());
			mainView.setVisible(false);
			waiterView.setVisible(true);
			waiterView.addBackListener(new WaiterBackListener());
			waiterView.addMoveMenuItemListener(new MoveMenuItemListener());
			waiterView.addCreateOrderListener(new CreateOrderListener());
			waiterView.addComputePriceListener(new ComputePriceListener());
			waiterView.addRemoveMenuItemListener(new RemoveMenuItemListener());
			waiterView.addShowOrderComponentsListener(new ShowOrderComponentsListener());
			waiterView.addGenerateBillListener(new GenerateBillListener());
			waiterView.addWindowCloseListener(new WindowCloseListener());
		}
		
	}
	
	class ChefBtnListener implements ActionListener{

		public void actionPerformed(ActionEvent arg) {
			mainView.setVisible(false);
			chefView.setVisible(true);
			chefView.addBackListener(new ChefBackListener());
			chefView.addWindowCloseListener(new WindowCloseListener());
		}
		
	}
	
	class AdminBackListener implements ActionListener{

		public void actionPerformed(ActionEvent arg) {
			adminView.setVisible(false);
			mainView.setVisible(true);
		}
		
	}
	
	class WaiterBackListener implements ActionListener{

		public void actionPerformed(ActionEvent arg) {
			waiterView.dispose();
			mainView.setVisible(true);
		}
		
	}
	
	class ChefBackListener implements ActionListener{

		public void actionPerformed(ActionEvent arg) {
			chefView.setVisible(false);
			mainView.setVisible(true);
		}
		
	}
	
	class MoveProductListener implements ActionListener{
		public void actionPerformed(ActionEvent arg) {
			String product = adminView.getProduct();
			if(product != null && !adminView.alreadyExists(product)) {
				adminView.addProduct(product);
			}
		}
	}
	
	class RemoveProductListener implements ActionListener{
		public void actionPerformed(ActionEvent arg) {
			adminView.removeProduct();
		}
	}
	
	class AddBaseProductListener implements ActionListener{
		public void actionPerformed(ActionEvent arg) {
			String name = adminView.getBaseProdName();
			float price = adminView.getBaseProdPrice();
			if(!name.equals("") && price != -1) {
				if(restaurant.createBaseMenuItem(name, price) == -1) {
					return;
				}
				adminView.createBaseMenuItem(name, price);
			}
		}
	}
	
	class DeleteBaseProductListener implements ActionListener{
		public void actionPerformed(ActionEvent arg) {
			
			String name = adminView.removeRow();
			if(!name.equals(" ")) {
				restaurant.deleteMenuItem(name, null);
				//delete products containing the deleted product
				ArrayList<String> itemsToRemove = restaurant.getItemsToRemove(name);
				adminView.deleteMenuItem(name, itemsToRemove);
			}
		}
	}
	
	class AddCompositeProductListener implements ActionListener{
		public void actionPerformed(ActionEvent arg) {
			String name = adminView.getCompositeProdName();
			ArrayList<String> components = adminView.getCompositeProdComponents();
			MenuItem product = restaurant.createCompositeMenuItem(name, 0, components);
			if(!name.equals("")) {
				if(product == null) {
					return;
				}
				float price = product.computePrice();
				adminView.createCompositeMenuItem(name, price, null);
			}
		}
	}
	
	class CompositeItemSelectedListener implements MouseListener{

		public void mouseClicked(MouseEvent arg) {
			String name = adminView.getSelectedProduct();
			ArrayList<String> components;
			components = restaurant.getComponents(name);
			adminView.addComponents(components);
		}

		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	class FillFieldsOnSelect implements MouseListener{

		public void mouseClicked(MouseEvent arg) {
			String name = adminView.getSelectedProduct();
			ArrayList<String> components = restaurant.getComponents(name);
			if(components.size() != 0) {
				adminView.setCompositeName(name);
				adminView.addComponentsOnSelect(components);
			} else {
				adminView.clearAddedProdList();
				adminView.setBaseName(name);
				String price = adminView.getSelectedProductPrice();
				adminView.setBasePrice(price);
			}
			
		}

		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	class EditItemListener implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			String name = adminView.getSelectedProduct();
			String baseName = adminView.getBaseProdName();
			String compositeName = adminView.getCompositeProdName();
			ArrayList<String> components = adminView.getComponentsFromList();
			float price = adminView.getBaseProdPrice();
			int c = restaurant.editItems(name, baseName, compositeName, price, components);
			if(c == 0) {
				adminView.editItems(name, baseName, "", restaurant.getPriceOfItem(baseName), null);
			} else if(c == 1) {
				adminView.editItems(name, compositeName, "", restaurant.getPriceOfItem(compositeName), null);
				adminView.addComponents(components);
			}
		}
		
	}
	
	class MoveMenuItemListener implements ActionListener{
		public void actionPerformed(ActionEvent arg) {
			String product = waiterView.getProduct();
			if(product != null && !waiterView.alreadyExists(product)) {
				waiterView.addProduct(product);
			}
		}
	}
	
	class RemoveMenuItemListener implements ActionListener{
		public void actionPerformed(ActionEvent arg) {
			waiterView.removeListItem();
		}
	}
	
	class CreateOrderListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String nb = waiterView.getTable();
			if(nb.equals("")) {
				return;
			}
			int table = Integer.parseInt(nb);
			String date = waiterView.getDate();
			ArrayList<String> items = waiterView.getMenuItems();
			restaurant.createNewOrder(counter, table, date, items);
			waiterView.createNewOrder(counter, table, date, items);
			counter++;
		}
		
	}
	
	class ComputePriceListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			int id = waiterView.getSelectedId();
			if(id == -1) {
				return;
			}
			float price = restaurant.computeOrderPrice(id, 0);
			waiterView.computeOrderPrice(id, price);
		}
		
	}
	
	class ShowOrderComponentsListener implements MouseListener{

		public void mouseClicked(MouseEvent arg) {
			int id = waiterView.getSelectedId();
			if(id == -1) {
				return;
			}
			ArrayList<String> components;
			components = restaurant.getOrderItems(id);
			if(components != null) {
				waiterView.addToOrderComponentsList(components);
			}
		}

		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	class GenerateBillListener implements ActionListener{

		public void actionPerformed(ActionEvent arg) {
			int id = waiterView.getSelectedId();
			if(id == -1) {
				return;
			}
			String date = waiterView.getSelectedDate();
			float price = restaurant.computeOrderPrice(id, 0);
			restaurant.generateBill(String.valueOf(id), date, price);
			waiterView.generateBill(String.valueOf(id), date, price);
		}
		
	}
	
	class WindowCloseListener implements WindowListener {

		public void windowActivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void windowClosed(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void windowClosing(WindowEvent arg0) {
			FileOutputStream fileOutputStream;
			try {
				fileOutputStream = new FileOutputStream("menu.txt");
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
		    objectOutputStream.writeObject(restaurant);
		    objectOutputStream.flush();
		    objectOutputStream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}		
		}

		public void windowDeactivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void windowDeiconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void windowIconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void windowOpened(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	void initAdminView() {
		ArrayList<MenuItem> menuItems = restaurant.getRestaurantMenu();
		for(MenuItem item: menuItems) {
			adminView.addToList(item.getName());
			adminView.addTableItem(item.getName(), String.valueOf(item.computePrice()));
		}
	}
	
	void initWaiterView() {
		ArrayList<Order> orders = restaurant.getOrders();
		for(Order order: orders) {
			waiterView.createNewOrder(order.getId(), order.getTable(), order.getDate(), null);
		}
	}
	
}
