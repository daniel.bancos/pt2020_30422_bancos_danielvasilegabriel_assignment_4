package mvc;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import bll.IRestaurantProcessing;
import bll.MenuItem;

public class AdministratorView extends JFrame implements IRestaurantProcessing{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton quitBtn = new JButton("Quit");
	private JButton addBaseProdBtn = new JButton("ADD");
	private JButton deleteBaseProdBtn = new JButton("DELETE");
	private JButton editBaseProdBtn = new JButton("EDIT");
	private JButton addCompositeProdBtn = new JButton("ADD");
	private JButton moveProdBtn = new JButton(">>");
	private JButton removeProdBtn = new JButton("X");
	
	private JPanel mainContent = new JPanel();
	private JPanel pane1 = new JPanel();
	private JPanel pane2 = new JPanel();
	private JPanel rightPanel = new JPanel();
	
	private JTextField baseProdNameTf = new JTextField();
	private JTextField baseProdPriceTf = new JTextField();
	private JTextField compositeProdNameTf = new JTextField();
	
	private JLabel baseNameLabel = new JLabel("NAME");
	private JLabel basePriceLabel = new JLabel("PRICE");
	private JLabel compositeNameLabel = new JLabel("NAME");
	
	private DefaultTableModel tableModel;
	private JTable productsTable = new JTable();
	
	
	DefaultListModel modelProdList = new DefaultListModel();
	DefaultListModel modelAddedProdList = new DefaultListModel();
	DefaultListModel modelComponentsList = new DefaultListModel();
	private JList prodList = new JList(modelProdList);
	private JList addedProdList = new JList(modelAddedProdList);
	private JList componentsList = new JList(modelComponentsList);
	
	AdministratorView(){
		
		pane1.setLayout(null);
		pane1.setBackground(Color.CYAN);
		pane1.setBounds(25, 30, 500, 295);
		pane1.add(addBaseProdBtn);
		pane1.add(baseNameLabel);
		pane1.add(basePriceLabel);
		pane1.add(baseProdNameTf);
		pane1.add(baseProdPriceTf);
		//name setup
		baseNameLabel.setBounds(10, 10, 50, 20);
		baseProdNameTf.setBounds(65, 10, 200, 25);
		//price setup
		basePriceLabel.setBounds(10, 40, 50, 20);
		baseProdPriceTf.setBounds(65, 40, 100, 25);
		//buttons for pane1
		addBaseProdBtn.setBounds(10, 100, 100, 40);
		
		pane2.setLayout(null);
		pane2.setBackground(Color.orange);
		pane2.setBounds(25, 345, 500, 295);
		pane2.add(compositeNameLabel);
		pane2.add(compositeProdNameTf);
		pane2.add(prodList);
		pane2.add(addedProdList);
		pane2.add(moveProdBtn);
		pane2.add(removeProdBtn);
		pane2.add(addCompositeProdBtn);
		compositeNameLabel.setBounds(10, 10, 50, 20);
		compositeProdNameTf.setBounds(65, 10, 200, 25);
		//modelProdList.addElement("meat");
		//modelProdList.addElement("potato");
		//modelProdList.addElement("lemon");
		//modelProdList.addElement("fish");
		prodList.setBounds(10, 70, 130, 150);
		prodList.setBorder(BorderFactory.createLineBorder(Color.black));
		prodList.setFont(new Font("Arial", Font.PLAIN, 19));
		addedProdList.setBounds(210, 70, 130, 150);
		addedProdList.setBorder(BorderFactory.createLineBorder(Color.black));
		addedProdList.setFont(new Font("Arial", Font.PLAIN, 19));
		//buttons for pane2
		addCompositeProdBtn.setBounds(10, 230, 100, 40);
		moveProdBtn.setBounds(150, 125, 50, 25);
		removeProdBtn.setBounds(350, 125, 50, 25);
		
		rightPanel.setLayout(null);
		rightPanel.setBackground(Color.DARK_GRAY);
		rightPanel.setBounds(550, 30, 420, 610);
		rightPanel.add(componentsList);
		rightPanel.add(deleteBaseProdBtn);
		rightPanel.add(editBaseProdBtn);
		deleteBaseProdBtn.setBounds(5, 80, 100, 40);
		editBaseProdBtn.setBounds(5, 130, 100, 40);
		productsTable.setBounds(110, 40, 200, 360);
		//set model for the products table
		tableModel = new DefaultTableModel() {
			@Override
	    public boolean isCellEditable(int row, int column) {
	       //all cells false
	       return false;
	    }
		};
		rightPanel.add(productsTable);
		productsTable.setModel(tableModel);
		componentsList.setBounds(110, 410, 200, 180);
		tableModel.addColumn("Name");
		tableModel.addColumn("Price");
		
		mainContent.setLayout(null);		
		mainContent.setBackground(Color.GRAY);
		
		quitBtn.setBounds(25, 645, 60, 20);
	
		mainContent.add(pane1);
		mainContent.add(pane2);
		mainContent.add(rightPanel);
		mainContent.add(quitBtn);
		
		//initView();
		
		this.setSize(1000, 700);
		this.setResizable(false);
		this.setPreferredSize(this.getSize());
		this.setContentPane(mainContent);
		this.pack();
		
		this.setLocationRelativeTo(null);
		this.setTitle("Admin");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public String getProduct() {
		if(!prodList.isSelectionEmpty()) {
			return prodList.getSelectedValue().toString();
		} else {
			return null;
		}
	}
	
	public void addProduct(String item) {
		modelAddedProdList.addElement(item);
	}
	
	public void removeProduct() {
		int index = addedProdList.getSelectedIndex();
		if(index != -1) {
			modelAddedProdList.remove(index);
		}
	}
	
	public void addTableItem(String name, String price) {
		tableModel.addRow(new Object[] {name, price});
	}
	
	public void deleteProduct(String product) {
		modelProdList.removeElement(product);
	}
	
	public boolean alreadyExists(String product) {
		
		for(int i = 0; i < modelAddedProdList.getSize(); i++) {
			String item = (String) modelAddedProdList.getElementAt(i);
			if(item.equals(product)) {
				return true;
			}
		}
		
		return false;
	}
	
	public void addToList(String product) {
		modelProdList.addElement(product);
	}
	
	public void clearAddedProdList() {
		modelAddedProdList.removeAllElements();
	}
	
	public String getBaseProdName() {
		return baseProdNameTf.getText();
	}
	
	public String getCompositeProdName() {
		return compositeProdNameTf.getText();
	}
	
	public String getSelectedProduct() {
		int row = productsTable.getSelectedRow();
		return tableModel.getValueAt(row, 0).toString();
	}
	
	public String getSelectedProductPrice() {
		int row = productsTable.getSelectedRow();
		return tableModel.getValueAt(row, 1).toString();
	}
	
	public ArrayList<String> getCompositeProdComponents(){
		ArrayList<String> result = new ArrayList();
		
		for(int i = 0; i < modelAddedProdList.getSize(); i++) {
			String item = modelAddedProdList.getElementAt(i).toString();
			result.add(item);
		}
		
		return result;
	}
	
	public String removeRow() {
		String name;
		int row = productsTable.getSelectedRow();
		
		if(row == -1) {
			return new String(" ");
		}
		
		name = tableModel.getValueAt(row, 0).toString();
		tableModel.removeRow(row);
		return name;
	}
	
	public void removeFromAll(ArrayList<String> itemsToRemove) {
		for(String item: itemsToRemove) {
			modelProdList.removeElement(item);
			if(getRow(item) != -1) {
				tableModel.removeRow(getRow(item));
			}
		}
	}
	
	private int getRow(String name) {
		for(int i = 0; i < tableModel.getRowCount(); i++) {
			if(tableModel.getValueAt(i, 0).toString().equals(name)) {
				return i;
			}
		}
		return -1;
	}
	
	public float getBaseProdPrice() {
		if(!baseProdPriceTf.getText().equals("")) {
			return Float.parseFloat(baseProdPriceTf.getText());
		} else {
			return -1;
		}
	}
	
	// this method fills a list with all the components of a selected composite item
	public void addComponents(ArrayList<String> components) {
		modelComponentsList.removeAllElements();
		for(String item: components) {
			modelComponentsList.addElement(item);
		}
	}
	
	public ArrayList<String> getComponentsFromList(){
		ArrayList<String> toReturn = new ArrayList();
		for(int i = 0; i < modelAddedProdList.getSize(); i++) {
			toReturn.add(modelAddedProdList.getElementAt(i).toString());
		}
		return toReturn;
	}
	
	// this method fills a list with all the components of a selected composite item
	// if future editing is wanted
	public void addComponentsOnSelect(ArrayList<String> components) {
		modelAddedProdList.removeAllElements();
		for(String item: components) {
			modelAddedProdList.addElement(item);
		}
	}
	
	public void editProduct(String oldName, String newName, float price) {
		modelProdList.removeElement(oldName);
		modelProdList.addElement(newName);
		for(int i = 0; i < tableModel.getRowCount(); i++) {
			if(tableModel.getValueAt(i, 0).equals(oldName)) {
				tableModel.setValueAt(newName, i, 0);
				tableModel.setValueAt(String.valueOf(price), i, 1);
				break;
			}
		}
	}
	
	public void setBaseName(String name) {
		baseProdNameTf.setText(name);
	}
	
	public void setBasePrice(String price) {
		baseProdPriceTf.setText(price);
	}
	
	public void setCompositeName(String name) {
		compositeProdNameTf.setText(name);
	}
	
	void addMoveProductListener(ActionListener action) {
		moveProdBtn.addActionListener(action);
	}
	
	void addRemoveProductListener(ActionListener action) {
		removeProdBtn.addActionListener(action);
	}
	
	void addBackListener(ActionListener action) {
		quitBtn.addActionListener(action);
	}
	
	void addAddBaseProdListener(ActionListener action) {
		addBaseProdBtn.addActionListener(action);
	}
	
	void addAddCompoisteProdListener(ActionListener action) {
		addCompositeProdBtn.addActionListener(action);
	}
	
	void addDeleteBaseProdListener(ActionListener action) {
		deleteBaseProdBtn.addActionListener(action);
	}
	
	void addCompositeItemSelectedListener(MouseListener action) {
		productsTable.addMouseListener(action);
	}
	
	void addEditItemListener(ActionListener action) {
		editBaseProdBtn.addActionListener(action);
	}
	
	void addWindowCloseListener(WindowListener action) {
		this.addWindowListener(action);
	}
	
	public void initView(ArrayList<MenuItem> items) {
		for(MenuItem item: items) {
			modelProdList.addElement(item.getName());
			tableModel.addRow(new String[] {""});
		}
	}

	public int createBaseMenuItem(String name, float price) {
		addToList(name);
		addTableItem(name, String.valueOf(price));
		return 1;
	}

	public MenuItem createCompositeMenuItem(String name, float price,ArrayList<String> items) {
		addToList(name);
		addTableItem(name, String.valueOf(price));
		return null;
	}

	public int editItems(String name, String baseName, String compositeName, float price, ArrayList<String> components) {
		editProduct(name, baseName, price);
		return 0;
	}

	public void deleteMenuItem(String name, ArrayList<String> itemsToRemove) {
		deleteProduct(name);
		removeFromAll(itemsToRemove);
		
	}

	public void createNewOrder(int id, int table, String date, ArrayList<String> items) {
		// TODO Auto-generated method stub
		
	}

	public float computeOrderPrice(int id, float price) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void generateBill(String id, String date, float price) {
		// TODO Auto-generated method stub
		
	}
}
