package mvc;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;

import javax.swing.*;

public class MainView extends JFrame{
	private JButton adminBtn = new JButton("Administrator");
	private JButton waiterBtn = new JButton("Waiter");
	private JButton chefBtn = new JButton("Chef");
	private JPanel mainContent = new JPanel();
	
	MainView(){
		
		adminBtn.setBackground(Color.decode("#9C4700"));
		adminBtn.setFont(new Font("Arial", Font.BOLD, 30));
		adminBtn.setForeground(Color.CYAN);
		
		waiterBtn.setBackground(Color.decode("#9C4700"));
		waiterBtn.setFont(new Font("Arial", Font.BOLD, 30));
		waiterBtn.setForeground(Color.CYAN);
		
		chefBtn.setBackground(Color.decode("#9C4700"));
		chefBtn.setFont(new Font("Arial", Font.BOLD, 30));
		chefBtn.setForeground(Color.CYAN);
		
		mainContent.setLayout(new GridLayout(0,1));
		mainContent.add(adminBtn);
		mainContent.add(waiterBtn);
		mainContent.add(chefBtn);
		
		this.setSize(300, 300);
		this.setResizable(false);
		this.setPreferredSize(this.getSize());
		this.setContentPane(mainContent);
		this.pack();
		
		this.setLocationRelativeTo(null);
		this.setTitle("Interface");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	void addAdminBtnListener(ActionListener action) {
		adminBtn.addActionListener(action);
	}
	
	void addWaiterBtnListener(ActionListener action) {
		waiterBtn.addActionListener(action);
	}
	
	void addChefBtnListener(ActionListener action) {
		chefBtn.addActionListener(action);
	}
	
	void addWindowCloseListener(WindowListener action) {
		this.addWindowListener(action);
	}
}
