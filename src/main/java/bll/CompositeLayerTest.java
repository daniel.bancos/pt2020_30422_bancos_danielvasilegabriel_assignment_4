package bll;

public class CompositeLayerTest {

	public static void main(String[] args) {
		MenuItem salata = new CompositeProduct("salata de pule");
		MenuItem fel2 = new CompositeProduct("cartofi cu carne");
		MenuItem fel20 = new CompositeProduct("mamaliga cu malai");
		
		
		MenuItem meniulZilei = new CompositeProduct("burta plina");
		
		meniulZilei.add(salata);
		meniulZilei.add(fel2);
		
		salata.add(new BaseProduct("rosie", 2));
		salata.add(new BaseProduct("ceapa", 4));
		
		fel2.add(new BaseProduct("piure", 10));
		fel2.add(new BaseProduct("carne", 20));
		
		fel20.add(new BaseProduct("mamaliga", 5));
		fel20.add(new BaseProduct("malai", 4));
		
		System.out.println("Meniul zilei price: " + meniulZilei.computePrice());
		System.out.println("fel20: " + fel20.computePrice());
	}

}
