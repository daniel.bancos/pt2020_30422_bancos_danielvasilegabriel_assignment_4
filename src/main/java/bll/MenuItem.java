package bll;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class MenuItem implements Serializable{
	
	public void add(MenuItem newItem) {
		throw new UnsupportedOperationException();
	}
	
	public void remove(MenuItem newItem) {
		throw new UnsupportedOperationException();
	}
	
	public MenuItem getMenuItem(int itemIndex) {
		throw new UnsupportedOperationException();
	}
	
	public float computePrice() {
		throw new UnsupportedOperationException();
	}
	
	public String getName() {
		throw new UnsupportedOperationException();
	}
	
	public void setName(String newName) {
		throw new UnsupportedOperationException();
	}
	
	public void setPrice(float newPrice) {
		throw new UnsupportedOperationException();
	}
	
	public ArrayList<String> getComponents(){
		throw new UnsupportedOperationException();
	}
	
	public boolean containsComponent(String name) {
		throw new UnsupportedOperationException();
	}
	
	public void clearComponents() {
		throw new UnsupportedOperationException();
	}
}
