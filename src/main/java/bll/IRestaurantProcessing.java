package bll;

import java.util.ArrayList;

public interface IRestaurantProcessing {
	
	/**
	 * creates a new base MenuItem
	 * @param name and price of the item
	 * @return 1 if succsessfull, 0 otherwise
	 * @pre name != "" && price > 0
	 * @post sizePost == sizePre + 1 || @nochange
	 */
	public int createBaseMenuItem(String name, float price);
	
	/**
	 * creates a new composite MenuItem
	 * @param name of item
	 * @param price of item
	 * @param components of item
	 * @return inserted object
	 * @pre name != "" && price > 0 && items.size() > 0
	 * @post sizePost == sizePre + 1 || @nochange
	 */
	public MenuItem createCompositeMenuItem(String name, float price,  ArrayList<String> items);
	
	/**
	 * edit an existing item
	 * @param name of the item to be edited
	 * @param new name
	 * @param new name
	 * @param new price of the item
	 * @param new list of components of the item
	 * @pre name != "" && baseName != ""
	 * @post @nochange
	 */
	public int editItems(String name, String baseName, String compositeName, float price, ArrayList<String> components);
	
	/**
	 * delete a MenuItem
	 * @param name of the item
	 * @param all the items that contain the item to delete
	 * @pre name != ""
	 * @post sizePost == sizePre - 1 || sizePost == sizePre
	 */
	public void deleteMenuItem(String name, ArrayList<String> itemsToRemove);
	
	/**
	 * creates a new order
	 * @param id of the order
	 * @param number of the table
	 * @param order date
	 * @param order items
	 * @pre id > 0 && table > 0 && items.size() > 0
	 * @post sizePost == sizePre + 1
	 */
	public void createNewOrder(int id, int table, String date, ArrayList<String> items);
	
	/**
	 * computes the price for an order
	 * @param id of the selected order
	 * @param the price of the item when the method is called in the waiterView frame
	 * @return the price of the order
	 * @pre id > 0 && price > 0
	 * @post @result > 0
	 */
	public float computeOrderPrice(int id, float price);
	
	/**
	 * generates a .txt file representing the bill for an order
	 * @param id of the selected order
	 * @param date of the selected order
	 * @param the total price of the selected order
	 * @pre Integer.parseInt(id) > 0 && price > 0
	 * @post @nochange
	 */
	public void generateBill(String id, String date, float price);
}
