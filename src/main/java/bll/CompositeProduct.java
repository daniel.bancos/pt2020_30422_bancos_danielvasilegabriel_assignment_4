package bll;

import java.util.ArrayList;
import java.util.Iterator;

public class CompositeProduct extends MenuItem {
	String name;
	ArrayList<MenuItem> items = new ArrayList<MenuItem>();
	
	public CompositeProduct(String newName) {
		name = newName;
	}
	
	public void add(MenuItem newItem) {
		items.add(newItem);
	}
	
	public void remove(MenuItem item) {
		items.remove(item);
	}
	
	public MenuItem getItem(int itemIndex) {
		return items.get(itemIndex);
	}
	
	public float computePrice() {
		float price = 0.0f;
		Iterator<MenuItem> itr = items.iterator();
		while(itr.hasNext()) {
			MenuItem item = itr.next();
			price += item.computePrice();
		}
		
		return price;
	}
	
	public String getName() {return name;}
	public void setName(String newName) {
		name = newName;
	}
	
	public ArrayList<String> getComponents(){
		ArrayList<String> components = new ArrayList();
		
		for(MenuItem item: items) {
			components.add(item.getName());
		}
		
		return components;
	}
	
	public boolean containsComponent(String name) {
		for(MenuItem it: items) {
			if(it.getName().equals(name)) {
				return true;
			}
		}
		
		return false;
	}
	
	public void clearComponents() {
		Iterator<MenuItem> itr = items.iterator();
		while(itr.hasNext()) {
			MenuItem item = itr.next();
			itr.remove();
		}
	}
}
