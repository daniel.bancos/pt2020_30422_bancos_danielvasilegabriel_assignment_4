package bll;

import java.util.Date;
import java.io.Serializable;

public class Order implements Serializable{
	private int id;
	private String date;
	private int table;
	
	public Order(int newId, String newDate, int newTable) {
		id = newId;
		date = newDate;
		table = newTable;
	}
	
	public int getId() {return id;}
	public String getDate() {return date;}
	public int getTable() {return table;}
	
	public int hashCode() {
		return id * table * date.hashCode();
	}
	
	
	public boolean equals(Order order) {
		if(id == order.getId()) {
			return true;
		} 
		
		return false;
	}
}
