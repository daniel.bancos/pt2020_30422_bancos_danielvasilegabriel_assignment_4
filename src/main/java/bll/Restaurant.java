package bll;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;

public class Restaurant extends Observable implements IRestaurantProcessing, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<MenuItem> restaurantMenu = new ArrayList<MenuItem>();
	Map<Order, ArrayList<MenuItem>> orders = new HashMap();
	private int billNb = 0;
	
	/**
	 * creates a new composite MenuItem
	 * @param name of item
	 * @param price of item
	 * @return 1 if succsessfull, 0 otherwise
	 */
	public int createBaseMenuItem(String name, float price) {
		assert name != "" && price > 0;
		int sizePre = restaurantMenu.size();
		for(MenuItem item: restaurantMenu) {
			if(item.getName().equals(name)) {
				return -1;
			}
		}
		MenuItem item = new BaseProduct(name, price);
		restaurantMenu.add(item);
		
		int sizePost = restaurantMenu.size();
		assert sizePost == sizePre + 1 || sizePost == sizePre;
		assert isWellFormed();
		return 0;
	}
	
	/**
	 * creates a new composite MenuItem
	 * @param name of item
	 * @param price of item
	 * @param components of item
	 * @return inserted object
	 */
	public MenuItem createCompositeMenuItem(String name, float price, ArrayList<String> items) {
		assert name != "" && items.size() > 0;
		int sizePre = restaurantMenu.size();
		
		MenuItem item = new CompositeProduct(name);
		for(MenuItem prod: restaurantMenu) {
			if(prod.getName().equals(name)) {
				return null;
			}
			if(existsInList(prod.getName(), items)) {
				item.add(prod);
			}
		}
		restaurantMenu.add(item);
		
		int sizePost = restaurantMenu.size();
		assert sizePost == sizePre + 1 || sizePost == sizePre;
		assert isWellFormed();
		return item;
	}
	
	private boolean existsInList(String name ,ArrayList<String> items) {
		for(String item: items) {
			if(item.equals(name)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * delete a MenuItem
	 * @param name of the item
	 * @param all the items that contain the item to delete
	 */
	public void deleteMenuItem(String name, ArrayList<String> itemsToRemove) {
		assert name != "";
		Iterator<MenuItem> itr = restaurantMenu.iterator();
		int sizePre = restaurantMenu.size();
		while(itr.hasNext()) {
			MenuItem item = itr.next();
			if(item.getName().equals(name)) {
				itr.remove();
				break;
			}
		}
		
		int sizePost = restaurantMenu.size();
		assert sizePost == sizePre - 1 || sizePost == sizePre;
		assert isWellFormed();
	}
	
	public ArrayList<String> getComponents(String name){
		ArrayList<String> components = new ArrayList();
		
		for(MenuItem item: restaurantMenu) {
			if(item.getName().equals(name)) {
				if(item.getClass().equals(CompositeProduct.class)) {
					components = item.getComponents();
					break;
				}
			}
		}
		
		return components;
	}
	
	public ArrayList<String> getItemsToRemove(String name){
		ArrayList<String> items = new ArrayList();
		
		Iterator<MenuItem> itr = restaurantMenu.iterator();
		while(itr.hasNext()) {
			MenuItem item = itr.next();
			if(item.containsComponent(name)) {
				items.add(item.getName());
				itr.remove();
			}
		}
		
		return items;
	}
	
	/**
	 * edit an existing item
	 * @param name of the item to be edited
	 * @param new name if base
	 * @param new name if composite
	 * @param new price of the item
	 * @param new list of components of the item
	 */
	public int editItems(String name, String baseName, String compositeName, float price, ArrayList<String> components) {
		assert name != "" && baseName != "";
		int sizePre = restaurantMenu.size();
		
		for(MenuItem item: restaurantMenu) {
			if(item.getName().equals(name)) {
				if(item.getClass().equals(BaseProduct.class)) {
					item.setName(baseName);
					item.setPrice(price);
					return 0;
				} else {
					//item = new CompositeProduct(compositeName);
					item.setName(compositeName);
					item.clearComponents();
					for(String st: components) {
						MenuItem toAdd = getMenuItem(st);
						item.add(toAdd);
					}
					return 1;
				}
			}
		}
		return -1;
	}
	
	private MenuItem getMenuItem(String name) {
		for(MenuItem item: restaurantMenu) {
			if(item.getName().equals(name)) {
				return item;
			}
		}
		return null;
	}
	
	public float getPriceOfItem(String name) {
		for(MenuItem item: restaurantMenu) {
			if(item.getName().equals(name)) {
				return item.computePrice();
			}
		}
		return -1;
	}
	
	/**
	 * creates a new order
	 * @param id of the order
	 * @param number of the table
	 * @param order date
	 * @param order items
	 */
	public void createNewOrder(int id, int table, String date, ArrayList<String> items) {
		assert id > 0 && table > 0 && items.size() > 0;
		int sizePre = orders.size();
		
		ArrayList<MenuItem> orderItems = new ArrayList();
		int compositeCheck = 0;
		for(String item: items) {
			MenuItem menuItem = getMenuItem(item);
			if(menuItem.getClass().equals(CompositeProduct.class)) {
				compositeCheck = 1;
			}
			orderItems.add(menuItem);
		}
		if(orderItems.isEmpty()) {
			return;
		}
		orders.put(new Order(id, date, table), orderItems);
		
		//notify chef if there is a composite item
		if(compositeCheck == 1) {
			ArrayList intel = new ArrayList();
			intel.add(new Order(id, date, table));
			intel.add(items);
			setChanged();
			notifyObservers(intel);
		}
		
		int sizePost = orders.size();
		assert sizePost == sizePre + 1;
		assert isWellFormed();
	}
	
	/**
	 * computes the price for an order
	 * @param id of the selected order
	 * @param the price of the item when the method is called in the waiterView frame
	 * @return the price of the order
	 */
	public float computeOrderPrice(int id, float price) {
		assert id > 0 && price > 0;
		
		float pr = 0;
		for(Order order: orders.keySet()) {
			if(order.getId() == id) {
				ArrayList<MenuItem> components = orders.get(order);
				for(MenuItem item: components) {
					pr += item.computePrice();
				}
			}
		}
		
		assert pr > 0;
		return pr;
	}
	
	public ArrayList<String> getOrderItems(int id){
		ArrayList<String> toReturn = new ArrayList();
		for(Order order: orders.keySet()) {
			if(order.getId() == id) {
				ArrayList<MenuItem> components = orders.get(order);
				components = orders.get(order);
				for(MenuItem item: components) {
					if(item != null) {
						toReturn.add(item.getName() + ", price: " + String.valueOf(item.computePrice()));
					}
				}
			}
		}
		return toReturn;
	}
	
	/**
	 * generates a .txt file representing the bill for an order
	 * @param id of the selected order
	 * @param date of the selected order
	 * @param the total price of the selected order
	 */
	public void generateBill(String id, String date, float price) {
		assert Integer.parseInt(id) > 0 && price > 0;
		
		String fileName = "Bill_" + billNb + ".txt";
		File bill = new File(fileName);
		billNb++;
		try {
			bill.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		String msg = "Order nb #" + id +", Date: " + date + ", Total: " + String.valueOf(price) +'\n';
		
    try {
    	FileWriter myWriter = new FileWriter(fileName);
			myWriter.write(msg);
			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<MenuItem> getRestaurantMenu(){
		return restaurantMenu;
	}
	
	public ArrayList<Order> getOrders(){
		ArrayList<Order> toReturn = new ArrayList();
		
		for(Order o: orders.keySet()) {
			toReturn.add(o);
		}
		
		return toReturn;
	}
	
	protected boolean isWellFormed() {
		
		if(restaurantMenu.size() == 0 && orders.size() != 0) {
			return false;
		}
		
		return true;
	}
}
