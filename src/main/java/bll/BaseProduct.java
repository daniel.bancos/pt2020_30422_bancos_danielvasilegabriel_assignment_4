package bll;

public class BaseProduct extends MenuItem {
	String name;
	float price;
	
	public BaseProduct(String newName, float newPrice) {
		name = newName;
		price = newPrice;
	}
	
	public String getName() {return name;}
	public void setName(String newName) {
		name = newName;
	}
	
	public float getPrice() {return price;}
	public void setPrice(float newPrice) {
		price = newPrice;
	}
	
	public float computePrice() {
		return price;
	}
	public boolean containsComponent(String name) {
		return false;
	}
}
